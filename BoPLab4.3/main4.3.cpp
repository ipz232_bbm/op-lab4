#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	double x, y;

	printf("x = ");
	scanf("%lf", &x);
	printf("y = ");
	scanf("%lf", &y);

	if (y >= 0 && x >= -5 && x <= 5 && y <= x * x) {
		printf("true");
		return 0;
	}
	printf("false");
	return 0;
}