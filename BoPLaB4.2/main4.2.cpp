#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	int a, b, c;

	printf("a = ");
	scanf("%d", &a);
	printf("b = ");
	scanf("%d", &b);
	printf("c = ");
	scanf("%d", &c);

	if (a % 2 == 0 || b % 2 == 0 || c % 2 == 0) {
		printf("%d", a >= b ? a >= c ? a : c : b >= c ? b : c);
		return 0;
	}
	printf("%d", a <= b ? a <= c ? a : c : b <= c ? b : c);

	return 0;
}