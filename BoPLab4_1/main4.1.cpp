﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <windows.h>
#include <Math.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	double a, b, c, x1, x2, D;

	printf("a = ");
	scanf("%lf", &a);
	printf("b = ");
	scanf("%lf", &b);
	printf("c = ");
	scanf("%lf", &c);

	printf("%fx^2 + (%f)x + (%f) = 0", a, b, c);

	if (a == 0) {
		if (b == 0) { printf("\nx є %s", c == 0 ? "R" : "{}"); return 0; }
		printf("\nx = %f", c == 0 ? 0 : -c / b); return 0;
	}

	D = b * b - 4 * a * c;
	
	if (D > 0) {
		x1 = (( - b + sqrt(D)) / 2 / a);
		x2 = (( - b - sqrt(D)) / 2 / a);
		printf("\nx1 = %f\nx2 = %f", x1, x2); 
		return 0;
	}

	D == 0 ? printf("\nx = %.f", -b / 2 / a) : printf("\nx є {}"); 

	return 0;
}